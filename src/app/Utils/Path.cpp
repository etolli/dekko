#include "Path.h"
#include <QStringList>
#include <QStandardPaths>
#include <QDir>
#include <QCoreApplication>

namespace Dekko {

QString findQmlFile(QString filePath) {
    QString qmlFile;
    QStringList paths = QStandardPaths::standardLocations(QStandardPaths::DataLocation);
    paths.prepend(QDir::currentPath());
    paths.prepend(QCoreApplication::applicationDirPath());
    Q_FOREACH (const QString &path, paths) {
        QString myPath = path + QLatin1Char('/') + filePath;
        if (QFile::exists(myPath)) {
            qmlFile = myPath;
            break;
        }
    }
    if (qmlFile.isEmpty()) {
        // ok not a standard location, lets see if we can find trojita.json, to see if we are a click package
        QString manifestPath = QDir(QCoreApplication::applicationDirPath()).absoluteFilePath(QLatin1String("../../dekko.json"));
        if (QFile::exists(manifestPath)) {
            // we are a click so lets tidy up our manifest path and find qmlfile
            QDir clickRoot = QFileInfo(manifestPath).absoluteDir();
            QString myPath = clickRoot.absolutePath() + QLatin1Char('/') + filePath;
            if (QFile::exists(myPath)) {
                qmlFile = myPath;
            }
        }
    }
    // sanity check
    if (qmlFile.isEmpty()) {
        qFatal("File: %s does not exist at any of the standard paths!", qPrintable(filePath));
    }
    return qmlFile;
}

}
