/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MailboxUtils.h"
#include <QDebug>
#include "Imap/Model/Model.h"
#include "Imap/Model/MailboxTree.h"
#include "Imap/Model/SubtreeModel.h"
#include "Imap/Model/CopyMoveOperation.h"

namespace Dekko
{
namespace Utils
{
MailboxUtils::MailboxUtils(QObject *parent) :
    QObject(parent), m_account(0)
{
}

QObject *MailboxUtils::account()
{
    return m_account;
}

void MailboxUtils::setAccount(QObject *account)
{
    m_account = qobject_cast<Accounts::Account *>(account);
    emit accountChanged();
}

bool MailboxUtils::markMessageAsRead(const QModelIndex &message, bool marked)
{
    Imap::Mailbox::Model *m_imapModel = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
    if (!m_imapModel)
        return false;
    m_imapModel->markMessagesRead(QModelIndexList() << message, marked ? Imap::Mailbox::FLAG_ADD : Imap::Mailbox::FLAG_REMOVE);
    return true;
}

bool MailboxUtils::markSelectedMessagesAsRead(bool marked)
{
    if (m_selectedIndexList.count()) {
        Imap::Mailbox::Model *m_imapModel = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
        if (m_imapModel) {
            m_imapModel->markMessagesRead(m_selectedIndexList, marked ? Imap::Mailbox::FLAG_ADD : Imap::Mailbox::FLAG_REMOVE);
            return true;
        }
    } else {
        qDebug() << "[MailboxUtils::markSelectedMessagesAsRead] No messages to mark";
    }
    return false;
}

bool MailboxUtils::markMessageDeleted(const QModelIndex &message, bool marked)
{
    Imap::Mailbox::Model *m_imapModel = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
    if (!m_imapModel)
        return false;
    m_imapModel->markMessagesDeleted(QModelIndexList() << message, marked ? Imap::Mailbox::FLAG_ADD : Imap::Mailbox::FLAG_REMOVE);
    return true;
}

bool MailboxUtils::markSelectedMessagesDeleted(bool mark)
{
    if (m_selectedIndexList.count()) {
        Imap::Mailbox::Model *m_imapModel = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
        if (m_imapModel) {
            m_imapModel->markMessagesDeleted(m_selectedIndexList, mark ? Imap::Mailbox::FLAG_ADD : Imap::Mailbox::FLAG_REMOVE);
            return true;
        }
    } else {
        qDebug() << "[MailboxUtils::markSelectedMessagesDeleted] No messages to marke deleted";
    }
    return false;
}

bool MailboxUtils::markMessageAsFlagged(const QModelIndex &message, bool marked)
{
    Imap::Mailbox::Model *m_imapModel = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
    if (!m_imapModel)
        return false;
    m_imapModel->setMessageFlags(QModelIndexList() << message, "\\Flagged", marked ? Imap::Mailbox::FLAG_ADD : Imap::Mailbox::FLAG_REMOVE);
    return true;
}

bool MailboxUtils::markSelectedMessagesFlagged(bool mark)
{
    if (m_selectedIndexList.count()) {
        Imap::Mailbox::Model *m_imapModel = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
        if (m_imapModel) {
            m_imapModel->setMessageFlags(m_selectedIndexList, "\\Flagged", mark ? Imap::Mailbox::FLAG_ADD : Imap::Mailbox::FLAG_REMOVE);
            return true;
        }
    } else {
        qDebug() << "[MailboxUtils::markSelectedMessagesFlagged] No messages to mark flagged";
    }
    return false;
}

QModelIndex MailboxUtils::deproxifiedIndex(const QModelIndex index)
{
    return Imap::deproxifiedIndex(index);
}

bool MailboxUtils::moveMessageToMailbox(const QString &currentMailbox, const uint &uid, const QString &targetMailbox)
{
    if (!m_account) {
        return false;
    }
    Imap::Mailbox::Model *m_model = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
    Q_ASSERT(m_model);
    if (!m_model->isNetworkAvailable()) {
        return false; // We need to have a network connection for this to work
    }
    Imap::Mailbox::TreeItemMailbox *currentMbox = static_cast<Imap::Mailbox::TreeItemMailbox *>(m_model->findMailboxByName(currentMailbox));
    if (!currentMbox) {
        return false;
    }
    // Ok let's move it now
    QList<uint> uids;
    uids << uid;
    m_model->copyMoveMessages(currentMbox, targetMailbox, uids, Imap::Mailbox::MOVE);
    return true;
}

bool MailboxUtils::expungeMailboxByName(const QString &mboxName)
{
    if (!m_account) {
        return false;
    }
    Imap::Mailbox::Model *m_model = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
    Q_ASSERT(m_model);
    if (!m_model->isNetworkAvailable()) {
    }
    QModelIndex currentMbox = static_cast<Imap::Mailbox::TreeItemMailbox *>(m_model->findMailboxByName(mboxName))->toIndex(m_model);
    if (!currentMbox.isValid()) {
        return false;
    }
    m_model->expungeMailbox(currentMbox);
    return true;
}

bool MailboxUtils::markMailboxAsRead(const QString &mboxName)
{
    if (!m_account) {
        return false;
    }
    Imap::Mailbox::Model *m_model = qobject_cast<Imap::Mailbox::Model *>(m_account->imapModel());
    Q_ASSERT(m_model);
    if (!m_model->isNetworkAvailable()) {
    }
    QModelIndex currentMbox = static_cast<Imap::Mailbox::TreeItemMailbox *>(m_model->findMailboxByName(mboxName))->toIndex(m_model);
    if (!currentMbox.isValid()) {
        return false;
    }
    m_model->markMailboxAsRead(currentMbox);
    return true;
}

void MailboxUtils::appendToSelectedIndexList(const QModelIndex &index)
{
    if (index.isValid()) {
        m_selectedIndexList.append(index);
        return;
    }
    qDebug() << "[MailboxUtils::appendToSelectedIndexList] Model index not valid";
}

void MailboxUtils::removeFromSelectedIndexList(const QModelIndex &index)
{
    if (index.isValid()) {
        if (m_selectedIndexList.contains(index)) {
            m_selectedIndexList.removeAll(index);
            return;
        }
        qDebug() << "[MailboxUtils::removeFromSelectedIndexList] Model index not in selected list";
    }
    qDebug() << "[MailboxUtils::removeFromSelectedIndexList] Model index not valid";
}

void MailboxUtils::clearSelectedMessages()
{
    if (m_selectedIndexList.count()) {
        m_selectedIndexList.clear();
        return;
    }
    qDebug() << "[MailboxUtils::clearSelectedMessages] called on an empty list";
}

}
}
