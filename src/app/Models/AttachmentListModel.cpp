/* Copyright (C) 2014-2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AttachmentListModel.h"
#include <QFileInfo>
#include <QMimeDatabase>
#include "UiUtils/Formatting.h"

namespace Dekko {

namespace Models {

Attachment::Attachment(QObject *parent, QString path)
    : QObject(parent)
{
    m_filePath = path;
}

QString Attachment::filePath()
{
    return m_filePath;
}

QString Attachment::caption()
{
    return QFileInfo(m_filePath).fileName();
}

QString Attachment::mimeTypeComment()
{
    QMimeDatabase db;
    QMimeType mime = db.mimeTypeForFile(caption());
    return mime.comment();
}

QString Attachment::mimeTypeGenericIconName()
{
    QMimeDatabase db;
    QMimeType mime = db.mimeTypeForFile(caption());
    return mime.genericIconName();
}

int Attachment::sizeInBytes()
{
    return QFileInfo(m_filePath).size();
}

AttachmentListModel::AttachmentListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

QHash<int, QByteArray> AttachmentListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[AttachmentCaptionRole] = "caption";
    roles[HumanReadableSizeRole] = "humanReadableSize";
    roles[MimeTypeRole] = "mimeType";
    roles[MimeTypeIconName] = "mimeTypeIconName";
    return roles;
}

QVariant AttachmentListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_attachmentList.size()) {
        return QVariant();
    }
    switch (role) {
    case AttachmentCaptionRole:
        return m_attachmentList[index.row()]->caption();
    case HumanReadableSizeRole:
        return UiUtils::Formatting::prettySize(m_attachmentList[index.row()]->sizeInBytes());
    case MimeTypeRole:
        return m_attachmentList[index.row()]->mimeTypeComment();
    case MimeTypeIconName:
        return m_attachmentList[index.row()]->mimeTypeGenericIconName();
    }
    return QVariant();
}

int AttachmentListModel::rowCount(const QModelIndex &parent) const
{
    return m_attachmentList.size();
}

void AttachmentListModel::addAttachment(const QString &path)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    Attachment* attachment = new Attachment(this, path);
    m_attachmentList.append(attachment);
    endInsertRows();
    emit countChanged();
}

void AttachmentListModel::removeAttachmentAtIndex(int idx)
{
    beginRemoveRows(QModelIndex(), idx, idx);
    m_attachmentList[idx]->deleteLater();
    m_attachmentList.removeAt(idx);
    endRemoveRows();
    emit countChanged();
}

void AttachmentListModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    for (int i = 0; i < m_attachmentList.count(); i++) {
        m_attachmentList[i]->deleteLater();
    }
    m_attachmentList.clear();
    endRemoveRows();
    emit countChanged();
}

Attachment *AttachmentListModel::getAttachmentAtIndex(int index)
{
    return m_attachmentList[index];
}

}
}
