/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 1.0
import "../Composer"
import "../Dialogs"
import TrojitaCore 0.1
import DekkoCore 0.2

Page {
    id: composePage
    property alias composerReplyMode: composer.replyingToMessage
    property alias replyMode: composer.replyMode
    property alias replyUid: replyToMessageModel.uid
    property alias replyMboxName: replyToMessageModel.mailbox
    property string pageTitle
    property Item pageTools
    property var initialReplyMode
    property string mailtoUrl

    title: pageTitle
    head.contents: Label {
        id: titleLabel
        width: parent ? parent.width : undefined
        anchors.verticalCenter: parent ? parent.verticalCenter : undefined
        text: composePage.title
        fontSize: "x-large"
        elide: Text.ElideRight
        wrapMode: Text.WordWrap
        maximumLineCount: fontSize === "large" ? 2 : 1
        onTruncatedChanged: {
            if (truncated) {
                fontSize = "large"
            }
        }
    }

    flickable: composePage.state == "panelOpen" ? composer : draftsPanel._listView

    Component.onCompleted: reloadComposer();
    function closeBottomEdge() {
        pageStack.pop();
    }

    function openDrafts() {
        draftsPanel.open()
    }

    function reloadComposer() {
        composer.resetComposer()
    }

    function writeTo(address) {
        if (replyMode != ReplyMode.REPLY_NONE) {
            initialReplyMode = replyMode;
            replyMode = ReplyMode.REPLY_NONE;
        }
        reloadComposer();
        composer._recipientField.createAddressObject(address, Recipient.ADDRESS_TO)
    }

    function addAttachments(files) {
        for (var i in files) {
            submissionManager.attachmentsModel.addAttachment(files[i].url.toString().replace("file://", ""));
        }
    }

    SubmissionManager {
        id: submissionManager
    }

    state: "panelOpen"
    states: [
        State {
            name: "panelOpen"
            PropertyChanges {
                target: composePage.head
                actions: composerHeader.actions
                backAction: composerHeader.backAction
            }
            PropertyChanges {
                target: composePage
                title:  composePage.pageTitle
            }
        },
        State {
            name: "draftsOpen"
            PropertyChanges {
                target: composePage.head
                actions: draftsHeader.actions
                backAction: draftsHeader.backAction
            }
            PropertyChanges {
                target: composePage
                pageTitle: qsTr("Drafts")
            }
        }
    ]

    ComposePageHeadConfiguration {
        id: composerHeader
        onSendClicked: {
            composer.sendEmail()
        }
        onCancelClicked: {
            Qt.inputMethod.hide()
            composer.loseFocus();
            //TODO: FIXME: only saving drafts for new messages atm
            if (composer.messageStarted) {
                PopupUtils.open(draftsDialog, dekko, {isEditingDraft: composer.docId})
            } else {
                closePanelTimer.start()
            }
        }
        onDraftsClicked: {
            draftsPanel.composer = true
            draftsPanel.open()
            composePage.state = "draftsOpen"
        }
    }

    Timer {
        id: closePanelTimer
        interval: 500
        onTriggered: closeBottomEdge();
    }
    DraftsPanel {
        id: draftsPanel
        anchors {
            left: parent.left
            bottom: parent.bottom
        }
        height: composePage.height
        width: parent.width
        property bool composer: false
        onOpenedChanged: opened ? composePage.state = "draftsOpen" : composePage.state = "panelOpen"
        onOpenDraft: {
            composer.openDraft(docId)
            // TODO: find out issue with deleting documents not removing list view delegates
            // re-enable once fixed
            composer.docId = docId
            composer.isEditingDraft = true
        }
        z: 10
    }

    MessageComposer {
        id: composer
        draftsDatabase: draftsPanel.draftsData
        onSubmissionSuceeded: {
            if (isEditingDraft) {
                draftsDatabase.deleteDoc(docId)
            }
            closePanelTimer.start();
        }
    }

    Component {
        id: draftsDialog
        DraftsDialog {
            onSaveDraft: {
                composer.saveNewDraft()
                closePanelTimer.start()
            }
            onDiscardDraft: {
                closePanelTimer.start()
            }
            onUpdateDraft: {
                composer.updateDraft()
                closePanelTimer.start()
            }
        }
    }

    PageHeadConfiguration {
        id: draftsHeader
        backAction: Action {
            iconName: "back"
            onTriggered: draftsPanel.composer ? draftsPanel.close() : composePage.closeBottomEdge()
        }
    }

    MessageModel {
        id: replyToMessageModel
        imapModel: dekko.currentAccount.imapModel
        Component.onCompleted: {
            if (mailbox && (uid != undefined)) {
                openMessage();
                messageTextContentFetcher.msgPartIndex = replyToMessageModel.mainTextPartIndex;
                composer.oneMessageModel = replyToMessageModel;
            }
        }
    }
    PlainTextFormatter {
        id: messageTextContentFetcher
        msgIndex: replyToMessageModel.messageIndex
        format: PlainTextFormatter.PLAIN
        onMessageChanged: {
            composer.message = rawMessage;
        }
    }
}
