/*
 * Copyright 2013 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Ubuntu.Components 1.1

Item {
    id: checkBoxStyle

    opacity: enabled ? 1.0 : 0.5

    implicitWidth: units.gu(4.25)
    implicitHeight: units.gu(4)
    Rectangle {
        id: background
        anchors.fill: parent
        anchors.margins: units.gu(0.5)
        border {
            color: UbuntuColors.coolGrey
            width: units.gu(0.3)
        }
        radius: units.gu(0.5)
    }

    Icon {
        id: tick
        name: "tick"
        width: parent.width - units.gu(2)
        height: width
        anchors.centerIn: parent
        visible: styledItem.checked || transitionToChecked.running || transitionToUnchecked.running
        smooth: true
        color: UbuntuColors.coolGrey
    }

    UbuntuShape {
        anchors.fill: parent
        anchors.margins: units.gu(0.5)
    }

    state: styledItem.checked ? "checked" : "unchecked"
    states: [
        State {
            name: "checked"
            PropertyChanges {
                target: tick
                anchors.verticalCenterOffset: 0
            }
        },
        State {
            name: "unchecked"
            PropertyChanges {
                target: tick
                anchors.verticalCenterOffset: checkBoxStyle.height
            }
        }
    ]

    transitions: [
        Transition {
            id: transitionToUnchecked
            to: "unchecked"
            SequentialAnimation {
                PropertyAction {
                    target: checkBoxStyle
                    property: "clip"
                    value: true
                }
                NumberAnimation {
                    target: tick
                    property: "anchors.verticalCenterOffset"
                    duration: UbuntuAnimation.FastDuration
                    easing: UbuntuAnimation.StandardEasingReverse
                }
                PropertyAction {
                    target: checkBoxStyle
                    property: "clip"
                    value: false
                }
            }
        },
        Transition {
            id: transitionToChecked
            to: "checked"
            SequentialAnimation {
                PropertyAction {
                    target: checkBoxStyle
                    property: "clip"
                    value: true
                }
                NumberAnimation {
                    target: tick
                    property: "anchors.verticalCenterOffset"
                    duration: UbuntuAnimation.FastDuration
                    easing: UbuntuAnimation.StandardEasing
                }
                PropertyAction {
                    target: checkBoxStyle
                    property: "clip"
                    value: false
                }
            }
        }
    ]
}
