/* Copyright (C) 2014-2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import QtContacts 5.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem

Repeater {
    id: root
    property var contact;

    signal emailSelected(var emailAddress, var displayLabel, var context);

    model: contact ? contact.details(ContactDetail.Email) : []
    delegate: ListItem.Subtitled {
        text: modelData.emailAddress
        subText: {
            if (modelData.contexts.indexOf(ContactDetail.ContextHome) > -1) {
                return "Home";
            } else if (modelData.contexts.indexOf(ContactDetail.ContextWork) > -1) {
                return "Work";
            } else if (modelData.contexts.indexOf(ContactDetail.ContextOther) > -1) {
                return "Other";
            } else {
                return "";
            }
        }
        showDivider: false
        onClicked: {
            root.clicked(modelData.emailAddress, item.displayLabel.label,
                         root.convertString2Context(recipientTypeSelectorListView.currentIndex));
        }
        Icon {
            width: height
            anchors {
                top: parent.top
                bottom: parent.bottom
                right: parent.right
                margins: units.gu(1.5)
            }
            name: "email"
        }
    }

    function convertString2Context(contextString) {
        if (contextString === "Home") {
            return ContactDetail.ContextHome;
        } else if (contextString === "Work") {
            return ContactDetail.ContextWork;
        } else {//Other
            return ContactDetail.ContextOther;
        }
    }

    function getSize() {
        return root.model.length
    }
}
