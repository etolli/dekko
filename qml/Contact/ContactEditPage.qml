/* Copyright (C) 2014-2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import QtContacts 5.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import "../Composer"

Page {
    id: root

    property var contact;
    property string prettyEmailAddress
    property alias name: nameInput.text
    property alias emailAddress: emailInput.text
    property var contactModel;

    flickable: scrollArea
    title: contact ? qsTr("Edit") : qsTr("Create")
    head {
        actions: [
            Action {
                iconName: "ok"
                onTriggered: {
                    if (state == "create") {
                        if (nameInput.text == "") {
                            PopupUtils.open(Qt.resolvedUrl("../Dialogs/InfoDialog.qml"), root, {
                                                title: qsTr("Error creating contact"),
                                                text: qsTr("Please fill in the name of the contact.")
                                            })
                            return;
                        }

                        contact = createContact(nameInput.text, emailInput.text,
                                                emailTypeSelector.getSelectedContext())
                        contactModel.saveContact(contact);
                        pageStack.pop();
                        pageStack.pop();
                    } else { // add
                        var emailDetail = createEmailDetail(emailInput.text, emailTypeSelector.getSelectedContext());
                        contact.addDetail(emailDetail);
                        contact.save();
                        pageStack.pop();
                        pageStack.pop();
                    }
                }
            }
        ]
    }
    state: contact === undefined ? "create" : "add"
    states: [
        State {
            name: "create"
            PropertyChanges {
                target: nameEditor
                visible: true
            }
            PropertyChanges {
                target: nameDisplay
                visible: false
            }
        },
        State {
            name: "add"
            PropertyChanges {
                target: nameEditor
                visible: false
            }
            PropertyChanges {
                target: nameDisplay
                visible: true
            }
        }

    ]

    Component.onCompleted: {
        //extract name from email address if there is any
        if (prettyEmailAddress.indexOf("<") > -1) {
            var components = prettyEmailAddress.split("<");
            console.log(prettyEmailAddress);
            root.emailAddress = components[1].split(">")[0];
            if (root.contact == undefined) {
                root.name = components[0].trim();
            }
        } else {
            root.emailAddress = prettyEmailAddress;
        }
    }

    Flickable {
        id: scrollArea
        clip: true
        height: parent.height
        width: parent.width
        flickableDirection: Flickable.VerticalFlick
        contentHeight: contents.height + units.gu(2)
        contentWidth: parent.width
        Column {
            id: contents

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            Column {
                id: nameEditor
                width: parent.width
                Item {
                    width: parent.width
                    height: units.gu(2)
                }
                PlainTextInput {
                    id: nameInput
                    placeholderText: qsTr("Name");
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(2)
                    }
                }
            }

            ListItem.Standard {
                id: nameDisplay
                text: root.contact !== undefined ? root.contact.displayLabel.label : ""
                showDivider: false
            }

            ListItem.Header {
                text: qsTr("New Email")
            }
            Item {
                width: parent.width
                height: units.gu(2)
            }

            RecipientInput{
                id: emailInput
                placeholderText: qsTr("Email");
                inputMethodHints: Qt.ImhNone
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
            }
            RecipientTypeSelector {
                id: emailTypeSelector
                values: [qsTr("Home"), qsTr("Work"), qsTr("Other")]
                readOnly: false
                active: true
                height: units.gu(4)
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: units.gu(2)
                }
                function getSelectedContext() {
                    var contextName = values[currentIndex];
                    if (contextName === "Home") {
                        return ContactDetail.ContextHome;
                    } else if (contextName === "Work") {
                        return ContactDetail.ContextWork;
                    } else {
                        return ContactDetail.ContextOther;
                    }
                }
            }

            Column {
                width: parent.width
                visible: root.state === "add" && listOfExsitingEmails.getSize() > 0
                ListItem.Header {
                    text: qsTr("Existing Emails")
                }
                EmailRepeater {
                    id: listOfExsitingEmails
                    contact: root.contact
                }
            }
        }
    }

    function createContact(name, email, emailType)
    {
        var newContact =  Qt.createQmlObject("import QtContacts 5.0; Contact{ }", root)
        var nameDetail = Qt.createQmlObject("import QtContacts 5.0; DisplayLabel {label: \"%1\";}"
                                        .arg(name), root);
        newContact.addDetail(nameDetail);

        var emailDetail = createEmailDetail(email, emailType);
        newContact.addDetail(emailDetail);

        return newContact;
    }
    function createEmailDetail(email, emailType) {
        var emailDetail = Qt.createQmlObject("import QtContacts 5.0; EmailAddress {emailAddress: \"%1\"}"
                                                .arg(email), root);
        emailDetail.contexts = [emailType];
        return emailDetail;
    }
}
