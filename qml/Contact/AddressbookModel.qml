/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import QtContacts 5.0
ContactModel {
    id: root

    property string filterOnValue
    property int capacity: 200 // maximal number of contact it stores

    manager: "memory"
    autoUpdate: true
    sortOrders: [
        SortOrder {
            detail: ContactDetail.Name
            field: Name.FirstName
            caseSensitivity: Qt.CaseInsensitive
        }
    ]
    filter: UnionFilter {
        filters: [
            DetailFilter {
                detail: ContactDetail.Name
                field: Name.FirstName
                matchFlags: DetailFilter.MatchContains
                value: filterOnValue
            },
            DetailFilter {
                detail: ContactDetail.Email
                field: EmailAddress.EmailAddress
                matchFlags: DetailFilter.MatchContains
                value: filterOnValue
            }]
    }

    function containsEmail(address) {
        var tmp = findEmailContact(address);
        return tmp !== false;
    }

    function saveEmail(name, address) {
        //find duplicate
        for (var i = 0; i < contacts.length; i++) {
            if (address == contacts[i].email.emailAddress) {
                return;
            }
        }
        saveContact(_createContact(name, address, ContactDetail.ContextOther));
        exportContacts(addressbookStoreLocation);
    }

    function removeEmail(address) {
        var contact = findEmailContact(address);
        if (contact) {
            removeContact(contact.contactId);
        }
        exportContacts(addressbookStoreLocation);
    }

    function findEmailContact(address) {
        var i;
        for (i = 0; i < contacts.length; i++) {
            if (contacts[i].email.emailAddress == address) {
                return contacts[i];
            }
        }
        return false;
    }

    function _createContact(name, email)
    {
        var newContact =  Qt.createQmlObject("import QtContacts 5.0; Contact{ }", root)
        if (name.length > 0) {
            var nameDetail = Qt.createQmlObject("import QtContacts 5.0; Name {firstName: \"%1\";}"
                                            .arg(name), root);
            newContact.addDetail(nameDetail);
        }
        var emailDetail = Qt.createQmlObject("import QtContacts 5.0; EmailAddress {emailAddress: \"%1\"}"
                                             .arg(email), root);
        newContact.addDetail(emailDetail);

        return newContact;
    }

    Component.onCompleted: importContacts(addressbookStoreLocation);

}
