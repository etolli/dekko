import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2
import "../../Components"
import "../../UCSComponents"
import "../common"

SettingsGroupBase {

    property var newIdentityPage

    function openNewIdentityPage() {
        var component = Qt.createComponent(Qt.resolvedUrl("NewIdentityPage.qml"))
        newIdentityPage = component.createObject(root)
        newIdentityPage.closing.connect(listView.forceLayout)
        pageStack.push(newIdentityPage, {model: identityModel})
    }

    SenderIdentityModel {
        id: identityModel
        accountId: root.account.accountId
        onCountChanged: saveIdentites()
    }

    ListView {
        id: listView
        // prevent binding loop for contentheight
        property int conHeight: contentHeight
        interactive: false
        height: conHeight
        anchors {
            left: parent.left
            right: parent.right
        }

        model: identityModel
        delegate: ListItem.Standard {
            removable: true
            confirmRemoval: true
            onItemRemoved: identityModel.removeRow(index)
            text: model.formattedString
        }

        footer: Item {
            visible: identityModel.count
            width: parent.width
            height: units.gu(7)
            Button {
                anchors {
                    centerIn: parent
                }
                visible: identityModel.count
                text: qsTr("Add identity")
                color: "green"
                width: units.gu(15)
                onClicked: openNewIdentityPage()
            }
        }
    }

    EmptyState {
        id: emptyState
        visible: !identityModel.count
        iconName: "contact"
        title: qsTr("No identities configured")
        anchors {
            left: parent.left
            top: parent.top
            topMargin: units.gu(15)
            right: parent.right
        }
    }

    Button {
        anchors {
            top: emptyState.bottom
            horizontalCenter: parent.horizontalCenter
        }
        visible: emptyState.visible
        text: qsTr("Add identity")
        color: "green"
        width: units.gu(15)
        onClicked: openNewIdentityPage()
    }
}
