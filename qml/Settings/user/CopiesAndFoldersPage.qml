import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2
import "../common"
import "../../Components"

SettingsGroupBase {

    Component.onDestruction: account.smtpSettings.save()
    Component.onCompleted: account = dekko.accountsManager.getFromId(root.account.accountId)

    property Account account

    Item {
        height: units.gu(3)
        width: parent.width
        Label {
            anchors {
                horizontalCenter: parent.horizontalCenter
                left: parent.left
            }
            text: qsTr("When sending messages:")
            font.weight: Font.DemiBold
        }
    }

    CheckboxWithLabel {
        id: placeCopyInCheckBox
        text: qsTr("Place a copy in:")
        checked: account.smtpSettings.saveToImap
        onCheckedChanged: account.smtpSettings.saveToImap = checked
    }

    MailboxFilterModel {
        id: filterModel
        // Eventually we are most likely going to have
        // to filter subscribed here, so we just set FILTER_NONE for the time being
        filterRole: MailboxFilterModel.FILTER_NONE
        sourceModel: account.mailboxModel
    }

    FlatteningProxyModel {
        id: flatModel
        sourceModel: filterModel
    }
    ListItem.ItemSelector {
        id: mailboxSelector
        expanded: false
        width: parent.width
        containerHeight: itemHeight * 6
        model: flatModel
        delegate: OptionSelectorDelegate {
            text: model.mailboxName
            onClicked: account.smtpSettings.sentMailboxName = model.mailboxName
            Component.onCompleted: model.mailboxName === account.smtpSettings.sentMailboxName ? mailboxSelector.selectedIndex = index : undefined
        }
    }
}
