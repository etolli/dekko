import QtQuick 2.3

ListModel {
    id: accSettingsModel

    Component.onCompleted: {
        append({ "description": qsTr("Incoming Server Settings"), "file": "ServerSettings.qml"})
        append({ "description": qsTr("Outgoing Server Settings"), "file": "OutgoingServerSettings.qml"})
        append({ "description": qsTr("Copies & Folders"), "file": "CopiesAndFoldersPage.qml"})
        append({ "description": qsTr("Sender Identities"), "file": "IdentitiesList.qml"})
        append({ "description": qsTr("Security"), "file": ""})
    }
}
