import QtQuick 2.3
import Ubuntu.Components 1.1
import DekkoCore 0.2

Page {
    id: newIdentityPage

    title: qsTr("New Identity")

    property QtObject model

    signal closing()

    head {
        actions: Action {
            id: saveAction
            iconName: "ok"
            onTriggered: {
                model.appendRow(newIdentity)
                closing()
                pageStack.pop()
            }
        }
    }

    SenderIdentity {
        id: newIdentity
        name: input.nameField
        email: input.emailField
        organization: input.organizationField
        signature: input.signatureField

    }
    Flickable {
        anchors.fill: parent
        contentHeight: input.height + units.gu(5)
        SenderIdentityInput {
            id: input
        }
    }

}
