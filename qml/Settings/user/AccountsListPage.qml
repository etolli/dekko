/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0
import DekkoCore 0.2
import "../../AccountsView"
import "../common"
import "../../Components"


Page {
    id: root
    title: qsTr("Accounts Settings")

    flickable: null
    property Account account
    // accountslistview emits this
    signal openAccount(int index)

    onOpenAccount: {
        account = dekko.accountsManager.get(index)
        state = "groups"
    }

    // On triggered, confirm dialog settings changed
    head.backAction: Action {
        iconName: "back"
        onTriggered: {
            if (state === "settings") {
                if (settingsView.accountSettingsChanged) {
                    // We need to confirm settings changed should be saved
                    var confirmDialog = PopupUtils.open(Qt.resolvedUrl("../../Dialogs/ConfirmationDialog.qml"), root, {title: qsTr("Save Changes?")})
                    confirmDialog.confirmClicked.connect(saveConfirmed)
                    confirmDialog.cancelClicked.connect(proceedToPreviousState)
                } else {
                    root.proceedToPreviousState();
                }

                //                state = "groups"
            } else {
                root.proceedToPreviousState()
            }
        }
    }

    function saveConfirmed() {
        settingsView.save()
    }

    function proceedToPreviousState() {
        if (state === "settings") {
            state = "groups"
            settingsView.accountSettingsChanged = false
            settingsView.destroyChildren()
        } else if (state === "groups") {
            state = "accounts"
        } else {
            pageStack.pop()
        }
    }

    state: "accounts"
    states: [
        State {
            name: "accounts"
            PropertyChanges {
                target: root
                title: qsTr("Accounts Settings")
            }
        },
        State {
            name: "groups"
            PropertyChanges {
                target: root
                title: qsTr("Account Settings")
            }
        },
        State {
            name: "settings"
        }
    ]

    // Step 1 choose account
    AccountsListView {
        id: accountsListView
        anchors.fill: parent
        visible: root.state === "accounts"
        listModel: dekko.accountsManager
    }

    //Step 2 choose settings group
    ListView {
        id: groupView
        clip: true
        anchors.fill: parent
        visible: root.state === "groups"
        model: SettingsGroupListModel {}
        delegate: ListItem.Standard {
            text: description
            onClicked: {
                settingsView.groupTitle = description
                settingsView.createSettingsObject(Qt.resolvedUrl(file))
            }
        }
    }

    //Step 3 edit settings
    SettingsGroupView {
        id: settingsView
        anchors.fill: parent
        visible: root.state === "settings"
        property bool accountSettingsChanged: false
        property string groupTitle: ""
        onSettingsChanged: accountSettingsChanged = changed
        onComponentLoaded: {
            root.state = "settings"
            root.title = groupTitle
        }
        onSaveComplete:root.proceedToPreviousState()
        onError: console.log(error)
    }
}
