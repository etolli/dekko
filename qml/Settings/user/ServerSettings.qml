import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2
import "../../Components"
import "../common"

SettingsGroupBase {

    onSave: {
        console.log("Save called")
        imapPassword.setPassword(imapPasswordField.text)
        imapSettings.connectionMethod = imapEncryptionSelector.method
        imapSettings.username = imapEmailField.text
        imapSettings.server = imapServerField.text
        imapSettings.port = imapPortField.text
        imapSettings.save()
        saveComplete()
    }

    property ImapSettings imapSettings: root.account.imapSettings
    // This is a crude hack to ensure we don't change the current saved port value
    // when the UI is loading. The encryption selector changes index twice during loading
    // which overwrites the portfield value if it isn't using a standard port for the connection method
    function __updatePortField(port) {
        imapPortField.text = imapEncryptionSelector.port
    }

    function determineIfSettingsChanged() {
        if (imapEncryptionSelector.method === imapSettings.connectionMethod
                && imapServerField.text === imapSettings.server
                && imapPortField.text === parseInt(imapSettings.port)
                && imapEmailField.text === imapSettings.username
                && imapPasswordField.text === imapPassword.currentPassword) {
            settingsChanged(false)
        } else {
            settingsChanged(true)
        }
    }

    EncryptionSelector {
        id: imapEncryptionSelector
        type: "imap"
        onMethodChanged: determineIfSettingsChanged()
        Component.onCompleted: {
            switch (imapSettings.connectionMethod) {
            case ImapSettings.NONE:
                selectedIndex = 0
                break
            case ImapSettings.STARTTLS:
                selectedIndex = 1
                break
            case ImapSettings.SSL:
                selectedIndex = 2
                break
            }
            // don't bind until selected index is set
            imapEncryptionSelector.onPortChanged.connect(__updatePortField)
        }
    }

    TitledTextField {
        id: imapServerField
        title: qsTr("Host name")
        text: imapSettings.server
        onTextChanged: determineIfSettingsChanged()
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: !text
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPortField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapPortField
        title: qsTr("Port")
        text: parseInt(imapSettings.port)
        onTextChanged: determineIfSettingsChanged()
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhDigitsOnly
        sendTabEventOnEnter: true
        requiredField: !text
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapEmailField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapEmailField
        title: qsTr("Username")
        text: imapSettings.username
        onTextChanged: determineIfSettingsChanged()
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: !text
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPasswordField.textFieldFocusHandle
    }

    PasswordManager {
        id: imapPassword
        property string currentPassword
        accountId: root.account.accountId
        type: PasswordManager.IMAP
        onPasswordNowAvailable: {
            currentPassword = passwordObject.password
            imapPasswordField.text = passwordObject.password
        }
        Component.onCompleted: requestPassword()
    }

    TitledTextField {
        id: imapPasswordField
        title: qsTr("Password")
        sendTabEventOnEnter: true
        onTextChanged: imapPassword.currentPassword ? determineIfSettingsChanged() : undefined
        inputMethodHints: Qt.ImhHiddenText | Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        echoMode: showImapPassword.checked ? TextInput.Normal : TextInput.Password
        KeyNavigation.priority: KeyNavigation.BeforeItem

    }

    CheckboxWithLabel {
        id: showImapPassword
        text: qsTr("Show password")
    }

}
