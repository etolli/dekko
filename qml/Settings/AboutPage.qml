/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import Ubuntu.Components.Popups 1.0

Page {
    id: root
    title: "About"

//    Action {
//        id: developerMode
//        onTriggered: currentAccount.developerModeEnabled = !currentAccount.developerModeEnabled
//    }

    Column {
        anchors.fill: parent
        anchors.topMargin: units.gu(7)

        Item {
            width: parent.width
            height: units.gu(6)
            Label {
                text: qsTr("Dekko Email Client")
                anchors.centerIn: parent
                fontSize: "x-large"
            }
        }

        Item {
            width: parent.width
            height: units.gu(20)

            UbuntuShape {
                radius: "medium"
                image: Image {
                    source: Qt.resolvedUrl("../../icons/dekko.svg")
                }
                height: units.gu(15); width: height
                anchors.centerIn: parent

//                MouseArea {
//                    anchors.fill: parent
//                    onPressAndHold: {
//                        PopupUtils.open(Qt.resolvedUrl("../Dialogs/ConfirmationDialog.qml"),
//                                        root,
//                                        {
//                                            confirmButtonText: currentAccount.developerModeEnabled ? "Disable" : "Enable",
//                                            confirmAction: developerMode,
//                                            title: "Developer Mode",
//                                            text: currentAccount.developerModeEnabled ? "Disable developer mode" : "Enable developer mode"
//                                        }
//                                        )
//                    }
//                }
            }
        }
//        Item {
//            width: parent.width
//            height: visible ? units.gu(5) : units.gu(0)
//            visible: currentAccount.developerModeEnabled
//            Label {
//                text: "Developer mode is enabled"
//                color: UbuntuColors.red
//                anchors.centerIn: parent
//            }
//        }

        Item {
            width: parent.width
            height: units.gu(5)
            Label {
                text: "Version 0.4"
                fontSize: "large"
                color: UbuntuColors.lightAubergine
                anchors.centerIn: parent
            }
        }
    }

}
