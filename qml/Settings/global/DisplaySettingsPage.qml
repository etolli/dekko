import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2

Page {
    id: displayPage

    title: qsTr("Display Settings")

    head {
        sections {
            model: ["General", "Reading", "Tags"]
        }
    }
    // Todo move to a General display settings column.
    OptionSelector {
        visible: head.sections.selectedIndex === 0
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            margins: units.gu(2)
        }
        text: qsTr("Application Theme")
        model: ["Ambiance", "SuruDark"]
        expanded: true
        onSelectedIndexChanged: {
            Theme.name = 'Ubuntu.Components.Themes.%1'.arg(model[selectedIndex])
            // Save it for next restart
            GlobalSettings.preferences.theme = Theme.name
        }
        Component.onCompleted: {
            if (GlobalSettings.preferences.theme === 'Ubuntu.Components.Themes.SuruDark') {
                selectedIndex = 1
            }
        }
    }

    DisplayReadingColumn {
        visible: head.sections.selectedIndex === 1
    }
}
