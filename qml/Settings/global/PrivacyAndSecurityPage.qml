import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2

Page {
    id: privAndSecPage
    title: qsTr("Privacy & Security")

    Column {
        id: col
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }
        ListItem.Standard {
            text: qsTr("Auto load images")
            control: Switch {
                onClicked: GlobalSettings.preferences.autoLoadImages = checked
                Component.onCompleted: checked = GlobalSettings.preferences.autoLoadImages
            }
        }
        ListItem.Standard {
            text: qsTr("Show you are using Dekko")
            control: Switch {
                onClicked: GlobalSettings.preferences.imapEnableId = checked
                Component.onCompleted: checked = GlobalSettings.preferences.imapEnableId
            }
        }
    }
}
