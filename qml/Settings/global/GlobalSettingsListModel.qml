import QtQuick 2.3

ListModel {
    id: globSettingsModel

    Component.onCompleted: {
        append({ "description": qsTr("Accounts"), "file": "../user/AccountsListPage.qml"})
        append({ "description": qsTr("Composition"), "file": "CompositionSettingsPage.qml"})
        append({ "description": qsTr("Display"), "file": "DisplaySettingsPage.qml"})
        append({ "description": qsTr("Offline"), "file": "OfflineSettingsPage.qml"})
        append({ "description": qsTr("Privacy & Security"), "file": "PrivacyAndSecurityPage.qml"})
        append({ "description": qsTr("Advanced"), "file": "AdvancedPage.qml"})
    }
}
