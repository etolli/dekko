import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2
import "../../Components"

Column {
    id: readingSettings
    anchors.fill: parent
    anchors.topMargin: units.gu(2)
    spacing: units.gu(1)

    OptionSelector {
        id: markAsReadSelector
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        text: qsTr("Automatically mark messages read")
        model: [qsTr("Never"), qsTr("After displaying for X seconds"), qsTr("Immediately")]
        delegate: OptionSelectorDelegate {
            text: modelData
        }
        onSelectedIndexChanged: {
            switch (selectedIndex) {
            case 0:
                GlobalSettings.preferences.markAsRead = Preferences.NEVER
                break;
            case 1:
                GlobalSettings.preferences.markAsRead = Preferences.AFTER_X_SECS
                break;
            case 2:
                GlobalSettings.preferences.markAsRead = Preferences.IMMEDIATELY
                break;
            }
        }
        Component.onCompleted: {
            selectedIndex = GlobalSettings.preferences.markAsRead
        }

    }

    TitledTextField {
        id: numSeconds
        anchors {
            left: parent.left
            right: parent.right
        }
        title: qsTr("Mark as read after displaying for")
        visible: markAsReadSelector.selectedIndex === 1
        onTextChanged: timer.start()
        Component.onCompleted: text = GlobalSettings.preferences.markAsReadAfter
        Timer {
            id: timer
            repeat: false
            interval: 500
            onTriggered: GlobalSettings.preferences.markAsReadAfter = numSeconds.text
        }
    }

    ListItem.Standard {
        text: qsTr("Prefer plain text messages")
        control: Switch {
            onClicked: GlobalSettings.preferences.preferPlainText = checked
            Component.onCompleted: checked = GlobalSettings.preferences.preferPlainText
        }
        showDivider: false
    }
    ListItem.Standard {
        text: qsTr("Hide messages marked for deletion")
        control: Switch {
            onClicked: GlobalSettings.preferences.hideMarkedDeleted = checked
            Component.onCompleted: checked = GlobalSettings.preferences.hideMarkedDeleted
        }
        showDivider: false
    }
}
