import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItem
import DekkoCore 0.2
import "../../Components"

Page {
    id: globalSettings
    title: qsTr("Settings")

    property string previousView

    head.backAction: Action {
        iconName: "back"
        onTriggered: {
            pageStack.clear()
            console.log("AccountsManagerCount", dekko.accountsManager.count)
            if (!dekko.accountsManager.count) {
                pageStack.push(Qt.resolvedUrl("../../AccountsView/AccountsPage.qml"))
                return;
            }

            if (previousView === "mbox") {
                pageStack.push(Qt.resolvedUrl("../../MailboxView/MailboxListPage.qml"))
            } else {
                pageStack.push(Qt.resolvedUrl("../../MessageListView/MessageListPage.qml"))
            }
        }
    }

    ListView {
        anchors.fill: parent
        model: GlobalSettingsListModel {}
        delegate: ListItem.Standard {
            text: description
            onClicked: pageStack.push(Qt.resolvedUrl(file))
        }

    }
}

