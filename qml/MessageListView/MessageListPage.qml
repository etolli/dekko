/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0 as ListItems
import Ubuntu.Components.Popups 1.0
import "../Components"
import "../Utils/Utils.js" as Utils
import "../UCSComponents"
import "../Composer"
import TrojitaCore 0.1
import DekkoCore 0.2

DekkoPage {
    id: messageListPage
    flickable: null
    title: messageListPage.state === "default" ? qsTr("Inbox") : qsTr("Edit")
    // This sets the DekkoPage's internal state to set the correct actions
    // from this context.
    pageType: "msgList"
    //-----------------------------------
    // PROPERTIES
    //-----------------------------------
    property string mailBox: dekko.currentAccount.msgListModel.mailboxName
    onMailBoxChanged: {
        if (messageListPage.state === "default") {
            if (mailBox === "INBOX") {
                title = qsTr("Inbox")
            } else {
                title = mailBox
            }
        }
    }
    head {
        contents: Row {
            height: parent.height
            spacing: units.gu(1)
            Label {
                anchors.verticalCenter: parent.verticalCenter
                text: messageListPage.title
                fontSize: "x-large"
            }
            Label {
                anchors {
                    verticalCenterOffset: units.gu(0.2)
                    verticalCenter: parent.verticalCenter
                }
                text: "(" + dekko.currentAccount.msgListModel.unreadCount + ")"
                fontSize: "large"
            }
        }

        sections {
            model: [qsTr("All"), qsTr("Unread"), qsTr("Starred")]
            enabled: state == "default" && !drawerOpen
        }
    }

    Connections {
        target: dekko.currentAccount.msgListModel
        onMailboxChanged: messagesListView.forceLayout()
    }

    headerActions: [
        Action {
            iconName: "search"
            visible: dekko.currentAccount.imapModel.isNetworkOnline
            onTriggered: {
                messageListPage.state = "search"
                searchComponentLoader.sourceComponent = searchComponent
            }
        },
        Action {
            id: connectionDialogAction
            iconName: "nm-no-connection"
            visible: !dekko.currentAccount.imapModel.isNetworkOnline
            text: qsTr("Network")
            onTriggered: {
                dekko.currentAccount.network.setNetworkOnline()
            }
        }
    ]
    state: "default"
    onStateChanged: {
        if (state === "edit") {
            editComponentLoader.sourceComponent = editComponent
        }

        console.log("StateChanged", state)
    }
    states: [
        PageHeadState {
            id: headerState
            name: "edit"
            head: messageListPage.head
            contents: Loader {
                id: editComponentLoader
                anchors {
                    left: parent ? parent.left : undefined
                    right: parent ? parent.right : undefined
                    rightMargin: units.gu(2)
                }
            }
            backAction: Action {
                text: "back"
                iconName: "back"
                onTriggered: {
                    messagesListView.cancelSelection()
                    editComponentLoader.sourceComponent = undefined
                    state = "default"
                }
            }
        },
        PageHeadState {
            id: searchState
            name: "search"
            head: messageListPage.head
            backAction: Action {
                text: "back"
                iconName: "back"
                onTriggered: {
                    mboxSearch.searchString = ""
                    mboxSearch.requestSearch() // This restores the list to unsorted state
                    messageListPage.state = "default"
                    searchComponentLoader.sourceComponent = undefined
                }
            }
            contents: Loader {
                id: searchComponentLoader
                anchors {
                    left: parent ? parent.left : undefined
                    right: parent ? parent.right : undefined
                    rightMargin: units.gu(2)
                }
            }
        }
    ]

    Component {
        id: editComponent
        MultiSelectToolbar {
            onVisibleChanged: {
                //To prevent a bug that display this on OneMessagePage
                if (!messageListPage.active)
                    visible = false;
            }
        }
    }

    Component {
        id: searchComponent
        TextField {
            id: searchField
            inputMethodHints: Qt.ImhNoPredictiveText
            placeholderText: qsTr("Search...")
            onAccepted: {
                focus = false
                mboxSearch.searchString = text
            }
            Component.onCompleted: focus = true
        }
    }
    MailboxSearch {
        id: mboxSearch
        msgModel: dekko.currentAccount.threadingModel ? dekko.currentAccount.threadingModel : null
        // Lets just support sender and subject as default and let the
        // user add body and recipient from the popover
        searchSender: true
        searchSubject: true
    }

    //----------------------------------
    // COMPONENTS
    //----------------------------------

    MessagesListView {
        id: messagesListView
        interactive: !drawerOpen
        height: parent.height
        width: parent.width
        clip: true
    }

    Connections {
        target: head.sections
        onSelectedIndexChanged: {
            // switch section wont remember selection now, so disable selection when user switch sections in edit mode.
            if (messagesListView.isInSelectionMode)
                messagesListView.cancelSelection();

            messagesListView.setFilter(head.sections.selectedIndex)
        }
    }

    RadialBottomEdge {
        visible: messageListPage.state !== "search" && dekko.currentAccount.imapModel.isNetworkOnline
        actions: [
            RadialAction {
                iconSource: "../../icons/actions/draft.svg"
                top: true
                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("../Composer/ComposePage.qml"),
                                   {
                                       replyMode: ReplyMode.REPLY_NONE,
                                       pageTitle:qsTr("New Message")
                                   });
                }
                text: qsTr("Compose")
            }
        ]
    }
}
