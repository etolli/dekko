/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0
import DekkoCore 0.2

OptionSelector {
    id: encryptionSelector

    property string type
    property int port
    property var method
    // TODO: make this *safer*, what about an ENUM for 'type'??
    onTypeChanged: type === "imap" ? encryptionMethodModel.loadImapModel() : encryptionMethodModel.loadSmtpModel()

    function updateValues() {
        method = model.get(selectedIndex).method
        port = model.get(selectedIndex).port
    }

    text: qsTr("Secure Connection")
    model: encryptionMethodModel
    showDivider: false
    expanded: false
    colourImage: true
    delegate: OptionSelectorDelegate { text: description }
    onSelectedIndexChanged: updateValues()

    ListModel {
        id: encryptionMethodModel

        function loadImapModel() {
            encryptionMethodModel.append({ "description": qsTr("No encryption"), "method": ImapSettings.NONE, "port": ImapSettings.PORT_IMAP })
            encryptionMethodModel.append({ "description": qsTr("Use encryption (STARTTLS)"), "method": ImapSettings.STARTTLS, "port": ImapSettings.PORT_IMAP })
            encryptionMethodModel.append({ "description": qsTr("Force encryption (SSL/TLS)"), "method": ImapSettings.SSL, "port": ImapSettings.PORT_IMAPS })
        }

        function loadSmtpModel() {
            encryptionMethodModel.append({ "description": qsTr("No encryption"), "method": SmtpSettings.SMTP, "port": SmtpSettings.PORT_SMTP_SUBMISSION })
            encryptionMethodModel.append({ "description": qsTr("Use encryption (STARTTLS)"), "method": SmtpSettings.SMTP_STARTTLS, "port": SmtpSettings.PORT_SMTP_SUBMISSION })
            encryptionMethodModel.append({ "description": qsTr("Force encryption (SSL/TLS)"), "method": SmtpSettings.SSMTP, "port": SmtpSettings.PORT_SMTP_SSL })
        }
    }

    Component.onCompleted: updateValues()
}

