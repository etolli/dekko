/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 1.0
import DekkoCore 0.2
import "../Components"

Column {
    id: column

    anchors {
        left: parent.left
        top: parent.top
        right: parent.right
    }

    property alias name: nameField.text
    property alias description: descriptionField.text
    property alias email: emailField.text
    property alias company: companyField.text
    property alias imapEncryptionMethod: imapEncryptionSelector.method
    property alias imapServer: imapServerField.text
    property string imapPort: imapPortField.text
    property alias imapUsername: imapEmailField.text
    property alias imapPassword: imapPasswordField.text
    property alias smtpEncryptionMethod: smtpEncryptionSelector.method
    property alias smtpServer: smtpServerField.text
    property alias smtpPort: smtpPortField.text
    property alias smtpUsername: smtpEmailField.text
    property alias smtpPassword: smtpPasswordField.text

    onVisibleChanged: {
        if (visible) {
            nameField.forceActiveFocus()
            nameField.textFieldFocusHandle.focus = true
        }
    }

    TitledTextField {
        id: nameField
        title: qsTr("Name")
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg: \"John Smith\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: descriptionField.textFieldFocusHandle
        font.capitalization: Font.Capitalize
    }

    TitledTextField {
        id: emailField
        title: qsTr("Email")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg: \"johnsmith@example.com\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPasswordField.textFieldFocusHandle
    }

    TitledTextField {
        id: descriptionField
        title: qsTr("Description")
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg: \"Personal\" or \"Work\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: companyField.textFieldFocusHandle
    }

    TitledTextField {
        id: companyField
        title: qsTr("Company (optional)")
        sendTabEventOnEnter: true
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapServerField.textFieldFocusHandle
    }

    Header {
        text: qsTr("Incoming Mail Server")
        width: dekko.width
    }

    EncryptionSelector {
        id: imapEncryptionSelector
        type: "imap"
        // We don't directly bind imapPort to this port
        // as they may want a non standard. So we only show the default when
        // a new encryption method has been selected
        onPortChanged: imapPortField.text = port
    }

    TitledTextField {
        id: imapServerField
        title: qsTr("Host name")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg: \"imap.example.com\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPortField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapPortField
        title: qsTr("Port")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhDigitsOnly
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapEmailField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapEmailField
        title: qsTr("Username")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: imapPasswordField.textFieldFocusHandle
    }

    TitledTextField {
        id: imapPasswordField
        title: qsTr("Password")
        sendTabEventOnEnter: true
        inputMethodHints: Qt.ImhHiddenText | Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        echoMode: showImapPassword.checked ? TextInput.Normal : TextInput.Password
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpServerField.textFieldFocusHandle
    }

    CheckboxWithLabel {
        id: showImapPassword
        text: qsTr("Show password")
    }

    Header {
        text: qsTr("Outgoing Mail Server")
    }

    EncryptionSelector {
        id: smtpEncryptionSelector
        type: "smtp"
        onPortChanged: smtpPortField.text = port
    }

    TitledTextField {
        id: smtpServerField
        title: qsTr("Host name")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        placeholderText: qsTr("Eg \"smtp.example.com\"")
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpPortField.textFieldFocusHandle
    }

    TitledTextField {
        id: smtpPortField
        title: qsTr("Port")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhDigitsOnly
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpEmailField.textFieldFocusHandle
    }

    TitledTextField {
        id: smtpEmailField
        title: qsTr("Username")
        inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhEmailCharactersOnly | Qt.ImhNoPredictiveText
        sendTabEventOnEnter: true
        requiredField: internal.invalidFields
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: smtpPasswordField.textFieldFocusHandle
    }

    TitledTextField {
        id: smtpPasswordField
        title: qsTr("Password")
        sendTabEventOnEnter: true
        inputMethodHints: Qt.ImhHiddenText | Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        echoMode: showSmtpPassword.checked ? TextInput.Normal : TextInput.Password
        KeyNavigation.priority: KeyNavigation.BeforeItem
        KeyNavigation.tab: descriptionField.textFieldFocusHandle
    }

    CheckboxWithLabel {
        id: showSmtpPassword
        text: qsTr("Show password")
    }
}
