#!/usr/bin/python3
from os.path import isdir, join, abspath, dirname
from sh import bzr, cd, git, rm, ErrorReturnCode

def syncWithLaunchpad():
    # Needs changing to sync with lp:dekko once move is finished
    bzr_branch = 'lp:~/dekko/sync'
    bzr_repo = join(dirname(abspath(__file__)), 'bzr-repo')
    
    print("Fetching all from git repo")
    git.fetch(all=True)
    
    print("Reseting origin/master")
    git.reset('--hard', 'origin/master')
    
    if isdir(bzr_repo):
        print("Removing old bzr repo: " + bzr_repo)
        rm(bzr_repo, r=True, f=True)
        
    print("Creating new bzr repo: " + bzr_repo)
    bzr('init-repo', 'bzr-repo')
    
    print("Moving to new bzr repo")
    cd(bzr_repo)
    
    print("Exporting git to bzr")
    bzr(git('-C', '../', 'fast-export', M=True, all=True), 'fast-import', '-')
    
    print("Uploading bzr repo to " + bzr_branch)
    bzr.push(bzr_branch, overwrite=True, directory="trunk")
    
    print("Upload complete")
    if isdir(bzr_repo):
        print("Removing local repo")
        rm(bzr_repo, r=True, f=True)

if __name__ == "__main__":
    syncWithLaunchpad()
